package paint;

import java.awt.BasicStroke;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.geom.*;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import static java.lang.Math.sqrt;
import static java.lang.Math.pow;

/**
 * A class to implement the drawing of a circle.
 */
public class Circle extends MyShape {

	private Shape shape;
	/**
	 * constructor to initialize the properties of a Circle.
	 */
	public Circle() {
		Map<String, Double> properties = new HashMap<String, Double>();
		final double initialLocation = 0.0;
		properties.put("type", 3.0);
		properties.put("x1", initialLocation);
		properties.put("y1", initialLocation);
		properties.put("x2", initialLocation);
		properties.put("y2", initialLocation);
		this.setProperties(properties);
	}

	/**
	 * constructor to put the actual properties of a Circle.
	 * 
	 * @param x1
	 *            x of the origin point.
	 * @param y1
	 *            y of the origin point.
	 * @param x2
	 *            x of the circumference point.
	 * @param y2
	 *            y of the circumference point.
	 */
	public Circle(final int x1, final int y1, final int x2, final int y2) {
		Map<String, Double> properties = new HashMap<String, Double>();
		properties.put("type", 3.0);
		properties.put("x1", (double) x1);
		properties.put("y1", (double) y1);
		properties.put("x2", (double) x2);
		properties.put("y2", (double) y2);
		this.setProperties(properties);
	}

	@Override
	final public void draw(Graphics canvas) {
		((Graphics2D) canvas).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		final int basicStroke = 4;
		((Graphics2D) canvas).setStroke(new BasicStroke(basicStroke));
		((Graphics2D) canvas).setPaint(this.getColor());
		Map<String, Double> properties = this.getProperties();
		final double centerX = properties.get("x1");
		final double centerY = properties.get("y1");
		final double circumX = properties.get("x2");
		final double circumY = properties.get("y2");
		final int square = 2;
		final double radius = sqrt(pow(circumX - centerX, square) + pow(circumY - centerY, square));
		final double cornerX = centerX - radius;
		final double cornerY = centerY - radius;
		final double Diameter = 2 * radius;
		Shape circle = new Ellipse2D.Double(cornerX, cornerY, Diameter, Diameter);
		((Graphics2D) canvas).draw(circle);
		((Graphics2D) canvas).setPaint(this.getFillColor());
		((Graphics2D) canvas).fill(circle);
		shape = circle;

	}

	@Override
	final public Object clone() throws CloneNotSupportedException {
		MyShape newCircle = new Circle();
		newCircle.setColor(this.getColor());
		newCircle.setFillColor(this.getFillColor());
		newCircle.setPosition(this.getPosition());
		HashMap<String, Double> newprop = new HashMap<String, Double>();
		for (Iterator<Entry<String, Double>> iterator = 
				this.getProperties().entrySet().iterator(); iterator.hasNext();) {
			Entry<String, Double> entry = iterator.next();
			newprop.put((String) entry.getKey(), (Double) entry.getValue());
		}
		newCircle.setProperties(newprop);
		return newCircle;
	}

	@Override
	public boolean contains(Point p) {
		if(shape != null){
			return shape.contains(p);
		}
		return false;
	}
}
