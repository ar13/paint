package paint;

import java.awt.AlphaComposite;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.SystemColor;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowStateListener;
import java.io.File;
import java.util.Map;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.filechooser.FileNameExtensionFilter;

import iPaint.Shape;

public class PaintGUI {

	private JFrame frame;
	private static boolean drag = false;
	private static Point dragLocation = new Point();
	private int shapeNum = 1;
	private Point drawStart, drawEnd, trianglePoint;
	private Color strokeColor = Color.BLACK, fillColor = Color.BLACK;
	private JButton undoBtn, redoBtn, fillBtn, strokeBtn;
	private MyDrawingEngine de = new MyDrawingEngine();
	private DrawingBoard board = new DrawingBoard();
	Graphics2D canvas;
	private Shape lastShape;
	private JPanel drawingPanel;
	private JLabel triLabel;
	private JPanel strokeColorPanel;
	private JPanel fillColorPanel;
	private boolean cutMode = false, copyMode = false, deleteMode = false;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PaintGUI window = new PaintGUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public PaintGUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setTitle("Painter");
		frame.setIconImage(Toolkit.getDefaultToolkit().getImage("./icon/paint.png"));
		frame.setBounds(100, 100, 800, 500);
		frame.setMinimumSize(new Dimension(800, 500));
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		JPanel floorPanel = new JPanel();
		floorPanel.setLayout(new FlowLayout(FlowLayout.LEFT, 3, 3));

		drawingPanel = new JPanel();
		drawingPanel.setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));
		drawingPanel.setBackground(Color.WHITE);
		drawingPanel.setPreferredSize(new Dimension(frame.getWidth() - 40, frame.getHeight() - 138));
		drawingPanel.setBorder(new LineBorder(Color.LIGHT_GRAY));
		drawingPanel.setLayout(new BorderLayout(0, 0));
		drawingPanel.add(board, BorderLayout.CENTER);
		floorPanel.add(drawingPanel);

		JScrollPane scrollPane = new JScrollPane(floorPanel);
		scrollPane.getHorizontalScrollBar().setUnitIncrement(50);
		scrollPane.getVerticalScrollBar().setUnitIncrement(50);

		scrollPane.setBackground(Color.WHITE);
		scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		scrollPane.setBounds(0, 75, 786, 386);
		JPanel contentPanel = new JPanel(null);
		contentPanel.setPreferredSize(new Dimension(500, 400));
		contentPanel.add(scrollPane);
		frame.setContentPane(contentPanel);

		JPanel managerPanel = new JPanel();
		managerPanel.setBackground(SystemColor.inactiveCaptionBorder);
		managerPanel.setBounds(0, 0, 786, 74);
		contentPanel.add(managerPanel);
		managerPanel.setLayout(null);

		JMenuBar menuBar = new JMenuBar();
		menuBar.setBounds(0, 0, 786, 24);
		managerPanel.add(menuBar);

		JMenu menuFile = new JMenu("File");
		menuFile.setFont(new Font("Calibri", menuFile.getFont().getStyle(), menuFile.getFont().getSize()));
		menuBar.add(menuFile);

		JMenuItem fileNew = new JMenuItem("New");
		fileNew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				PaintGUI newWindow = new PaintGUI();
				newWindow.frame.setVisible(true);
			}
		});
		fileNew.setFont(new Font("Calibri", fileNew.getFont().getStyle(), fileNew.getFont().getSize()));
		Icon fileNewic = new ImageIcon("./icon/new.png");
		fileNew.setIcon(fileNewic);
		menuFile.add(fileNew);

		JMenuItem fileOpen = new JMenuItem("Open File ...");
		fileOpen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JFileChooser fileChooser = new JFileChooser();
				fileChooser.setCurrentDirectory(new File(System.getProperty("user.home")));
				FileNameExtensionFilter filter1 = new FileNameExtensionFilter("JSON", "json");
				FileNameExtensionFilter filter2 = new FileNameExtensionFilter("xml", "xml");
				fileChooser.addChoosableFileFilter(filter1);
				fileChooser.addChoosableFileFilter(filter2);
				fileChooser.setAcceptAllFileFilterUsed(false);
				final int result = fileChooser.showOpenDialog(frame);
				if (result == JFileChooser.APPROVE_OPTION) {
					final String filePath = fileChooser.getSelectedFile().getAbsolutePath();
					//System.out.println("File Path: " + filePath);
					/*
					 * 
					 * progress ......
					 *  
					 */
					redoBtn.setEnabled(false);
					undoBtn.setEnabled(false);
					de.load(filePath);
					board.repaint();
				}
				
			}
		});
		fileOpen.setFont(new Font("Calibri", fileOpen.getFont().getStyle(), fileOpen.getFont().getSize()));
		Icon fileOpenic = new ImageIcon("./icon/open.png");
		fileOpen.setIcon(fileOpenic);
		menuFile.add(fileOpen);

		JMenuItem fileSave = new JMenuItem("Save As ...");
		fileSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser fileChooser = new JFileChooser();
				fileChooser.setCurrentDirectory(new File(System.getProperty("user.home")));
				FileNameExtensionFilter filter1 = new FileNameExtensionFilter("JSON", "json");
				FileNameExtensionFilter filter2 = new FileNameExtensionFilter("xml", "xml");
				fileChooser.setFileFilter(filter1);
				fileChooser.setFileFilter(filter2);
				fileChooser.setAcceptAllFileFilterUsed(false);
				final int result = fileChooser.showSaveDialog(frame);
				if (result == JFileChooser.APPROVE_OPTION) {
					String filePath = "";
					if (fileChooser.getFileFilter().equals(filter1)) {
						filePath = (fileChooser.getSelectedFile().getAbsolutePath() + ".json");	
					} else if (fileChooser.getFileFilter().equals(filter2)){
						filePath = (fileChooser.getSelectedFile().getAbsolutePath() + ".xml");
					}
					//System.out.println("File Path: " + filePath);
					de.save(filePath);
				}
			}
		});
		fileSave.setFont(new Font("Calibri", fileSave.getFont().getStyle(), fileSave.getFont().getSize()));
		Icon fileSaveic = new ImageIcon("./icon/save.png");
		fileSave.setIcon(fileSaveic);
		menuFile.add(fileSave);

		JSeparator separator = new JSeparator();
		menuFile.add(separator);

		JMenuItem fileExit = new JMenuItem("Exit");
		fileExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frame.dispose();
			}
		});
		fileExit.setFont(new Font("Calibri", fileExit.getFont().getStyle(), fileExit.getFont().getSize()));
		Icon fileExitic = new ImageIcon("./icon/exit.png");
		fileExit.setIcon(fileExitic);
		menuFile.add(fileExit);

		JMenu menuEdit = new JMenu("Edit");
		menuEdit.setFont(new Font("Calibri", menuEdit.getFont().getStyle(), menuEdit.getFont().getSize()));
		menuBar.add(menuEdit);

		JMenuItem editUndo = new JMenuItem("Undo");
		editUndo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				undoBtn.doClick();
			}
		});
		editUndo.setFont(new Font("Calibri", editUndo.getFont().getStyle(), editUndo.getFont().getSize()));
		Icon editUndoic = new ImageIcon("./icon/undo.png");
		editUndo.setIcon(editUndoic);
		menuEdit.add(editUndo);

		JMenuItem editRedo = new JMenuItem("Redo");
		editRedo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				redoBtn.doClick();
			}
		});
		editRedo.setFont(new Font("Calibri", editRedo.getFont().getStyle(), editRedo.getFont().getSize()));
		Icon editRedoic = new ImageIcon("./icon/redo.png");
		editRedo.setIcon(editRedoic);
		menuEdit.add(editRedo);

		JSeparator separator_1 = new JSeparator();
		menuEdit.add(separator_1);

		JMenuItem editCut = new JMenuItem("Cut");
		editCut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cutMode = true;
				copyMode = deleteMode = false;
			}
		});
		editCut.setFont(new Font("Calibri", editCut.getFont().getStyle(), editCut.getFont().getSize()));
		Icon editCutic = new ImageIcon("./icon/cut.png");
		editCut.setIcon(editCutic);
		menuEdit.add(editCut);

		JMenuItem editCopy = new JMenuItem("Copy");
		editCopy.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				copyMode = true;
				cutMode = deleteMode = false;
			}
		});
		editCopy.setFont(new Font("Calibri", editCopy.getFont().getStyle(), editCopy.getFont().getSize()));
		Icon editCopyic = new ImageIcon("./icon/copy.png");
		editCopy.setIcon(editCopyic);
		menuEdit.add(editCopy);

		JMenuItem editPaste = new JMenuItem("Paste");
		editPaste.setFont(new Font("Calibri", editPaste.getFont().getStyle(), editPaste.getFont().getSize()));
		Icon editPasteic = new ImageIcon("./icon/paste.png");
		editPaste.setIcon(editPasteic);
		menuEdit.add(editPaste);

		JSeparator separator_2 = new JSeparator();
		menuEdit.add(separator_2);

		JMenuItem editDelete = new JMenuItem("Delete");
		editDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				deleteMode = true;
				cutMode = copyMode = false;
			}
		});
		editDelete.setFont(new Font("Calibri", editDelete.getFont().getStyle(), editDelete.getFont().getSize()));
		Icon editDeleteic = new ImageIcon("./icon/delete.png");
		editDelete.setIcon(editDeleteic);
		menuEdit.add(editDelete);

		JPanel toolPanel = new JPanel();
		toolPanel.setBounds(0, 24, 786, 50);
		managerPanel.add(toolPanel);
		toolPanel.setLayout(null);

		JButton lineBtn = new JButton();
		lineBtn.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				shapeNum = 1;
				triLabel.setText("");
				copyMode = cutMode = deleteMode = false;
			}
		});
		lineBtn.setBackground(Color.WHITE);
		JButton ellipseBtn = new JButton();
		ellipseBtn.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				shapeNum = 2;
				triLabel.setText("");
				copyMode = cutMode = deleteMode = false;
			}
		});
		ellipseBtn.setBackground(Color.WHITE);
		JButton circleBtn = new JButton();
		circleBtn.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				shapeNum = 3;
				triLabel.setText("");
				copyMode = cutMode = deleteMode = false;
			}
		});
		circleBtn.setBackground(Color.WHITE);
		JButton triangleBtn = new JButton();
		triangleBtn.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				shapeNum = 4;
				triLabel.setText("Tap 3 points to form the Triangle");
				copyMode = cutMode = deleteMode = false;
			}
		});
		triangleBtn.setBackground(Color.WHITE);
		JButton rectangleBtn = new JButton();
		rectangleBtn.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				shapeNum = 5;
				triLabel.setText("");
				copyMode = cutMode = deleteMode = false;
			}
		});
		rectangleBtn.setBackground(Color.WHITE);
		JButton squareBtn = new JButton();
		squareBtn.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				shapeNum = 6;
				triLabel.setText("");
				copyMode = cutMode = deleteMode = false;
			}
		});
		squareBtn.setBackground(Color.WHITE);

		Icon lineIc = new ImageIcon("./icon/line.png");
		Icon ellipseIc = new ImageIcon("./icon/ellipse.png");
		Icon circleIc = new ImageIcon("./icon/circle.png");
		Icon triangleIc = new ImageIcon("./icon/triangle.png");
		Icon rectangleIc = new ImageIcon("./icon/rectangle.png");
		Icon squareIc = new ImageIcon("./icon/square.png");

		lineBtn.setIcon(lineIc);
		ellipseBtn.setIcon(ellipseIc);
		circleBtn.setIcon(circleIc);
		triangleBtn.setIcon(triangleIc);
		rectangleBtn.setIcon(rectangleIc);
		squareBtn.setIcon(squareIc);

		lineBtn.setFocusable(false);
		ellipseBtn.setFocusable(false);
		circleBtn.setFocusable(false);
		triangleBtn.setFocusable(false);
		rectangleBtn.setFocusable(false);
		squareBtn.setFocusable(false);

		lineBtn.setToolTipText("Line");
		ellipseBtn.setToolTipText("Ellipse");
		circleBtn.setToolTipText("Circle");
		triangleBtn.setToolTipText("Triangle");
		rectangleBtn.setToolTipText("Rectangle");
		squareBtn.setToolTipText("Square");

		undoBtn = new JButton();
		undoBtn.setBorder(new LineBorder(new Color(0, 0, 0)));
		undoBtn.setEnabled(false);
		undoBtn.setToolTipText("undo");
		undoBtn.setBackground(Color.WHITE);
		undoBtn.setBounds(128, 3, 46, 46);
		undoBtn.setIcon(editUndoic);
		toolPanel.add(undoBtn);

		undoBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				de.undo();
				board.repaint();
				redoBtn.setEnabled(true);
				if (de.undoListEmpty()) {
					undoBtn.setEnabled(false);
				}
			}
		});

		redoBtn = new JButton();
		redoBtn.setBorder(new LineBorder(new Color(0, 0, 0)));
		redoBtn.setEnabled(false);
		redoBtn.setToolTipText("redo");
		redoBtn.setBackground(Color.WHITE);
		redoBtn.setBounds(178, 3, 46, 46);
		redoBtn.setIcon(editRedoic);
		toolPanel.add(redoBtn);

		redoBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				de.redo();
				board.repaint();
				undoBtn.setEnabled(true);
				if (de.redoListEmpty()) {
					redoBtn.setEnabled(false);
				}
			}
		});

		JSeparator separator_3 = new JSeparator();
		separator_3.setBackground(Color.BLACK);
		separator_3.setForeground(Color.BLACK);
		separator_3.setOrientation(SwingConstants.VERTICAL);
		separator_3.setBounds(117, 3, 1, 48);
		toolPanel.add(separator_3);

		JSeparator separator_4 = new JSeparator();
		separator_4.setOrientation(SwingConstants.VERTICAL);
		separator_4.setForeground(Color.BLACK);
		separator_4.setBackground(Color.BLACK);
		separator_4.setBounds(234, 3, 1, 48);
		toolPanel.add(separator_4);

		fillBtn = new JButton();
		fillBtn.setBorder(new LineBorder(new Color(0, 0, 0)));
		fillBtn.setToolTipText("fill color");
		fillBtn.setBackground(Color.WHITE);
		fillBtn.setBounds(245, 26, 22, 23);
		Icon fillIc = new ImageIcon("./icon/fill.png");
		fillBtn.setIcon(fillIc);
		toolPanel.add(fillBtn);

		fillBtn.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				Color tempColor = JColorChooser.showDialog(null, "Pick a Fill Color", fillColor);
				if (tempColor == null) {
					tempColor = fillColor;
				}
				fillColor = tempColor;
				fillColorPanel.setBackground(fillColor);
			}
		});

		strokeBtn = new JButton();
		strokeBtn.setBorder(new LineBorder(new Color(0, 0, 0)));
		strokeBtn.setToolTipText("stroke color");
		strokeBtn.setBackground(Color.WHITE);
		strokeBtn.setBounds(245, 3, 22, 23);
		Icon strokeIc = new ImageIcon("./icon/stroke.png");
		strokeBtn.setIcon(strokeIc);
		toolPanel.add(strokeBtn);

		strokeBtn.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				Color tempColor = JColorChooser.showDialog(null, "Pick a Stroke", strokeColor);
				if (tempColor == null) {
					tempColor = strokeColor;
				}
				strokeColor = tempColor;
				strokeColorPanel.setBackground(strokeColor);
			}
		});

		JPanel floorPanel1 = new JPanel();
		floorPanel1.setPreferredSize(new Dimension(200, 28));
		floorPanel1.setLayout(new FlowLayout(FlowLayout.LEFT, 0, 0));

		JPanel shapePanel1 = new JPanel();
		shapePanel1.setBackground(Color.WHITE);
		shapePanel1.setPreferredSize(new Dimension(150, 30));
		shapePanel1.setLayout(new GridLayout(1, 10, 0, 0));

		shapePanel1.add(lineBtn);
		shapePanel1.add(ellipseBtn);
		shapePanel1.add(circleBtn);
		shapePanel1.add(triangleBtn);
		shapePanel1.add(rectangleBtn);
		shapePanel1.add(squareBtn);

		floorPanel1.add(shapePanel1);

		JScrollPane scrollPane1 = new JScrollPane(floorPanel1);
		scrollPane1.setBorder(null);
		scrollPane1.getHorizontalScrollBar().setUnitIncrement(50);
		scrollPane1.setBackground(Color.WHITE);
		scrollPane1.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scrollPane1.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		scrollPane1.setBounds(0, 0, 103, 46);

		JPanel contentPanel1 = new JPanel();
		contentPanel1.setLayout(null);
		contentPanel1.setPreferredSize(new Dimension(84, 46));
		contentPanel1.setBackground(Color.WHITE);
		contentPanel1.setBounds(5, 3, 103, 46);
		contentPanel1.add(scrollPane1);
		toolPanel.add(contentPanel1);

		triLabel = new JLabel("");
		triLabel.setFont(new Font("Calibri", triLabel.getFont().getStyle(), triLabel.getFont().getSize()));
		triLabel.setBounds(455, 26, 196, 14);
		toolPanel.add(triLabel);

		strokeColorPanel = new JPanel();
		strokeColorPanel.setBorder(new LineBorder(new Color(0, 0, 0)));
		strokeColorPanel.setBackground(Color.BLACK);
		strokeColorPanel.setBounds(270, 3, 22, 21);
		toolPanel.add(strokeColorPanel);

		fillColorPanel = new JPanel();
		fillColorPanel.setBorder(new LineBorder(new Color(0, 0, 0)));
		fillColorPanel.setBackground(Color.BLACK);
		fillColorPanel.setBounds(270, 28, 22, 21);
		toolPanel.add(fillColorPanel);

		frame.pack();
		frame.setVisible(true);

		frame.addComponentListener(new ComponentAdapter() {
			public void componentResized(ComponentEvent event) {
				managerPanel.setBounds(0, 0, frame.getWidth() - 14, 74);
				scrollPane.setBounds(0, 75, frame.getWidth() - 14, frame.getHeight() - 114);
				menuBar.setBounds(0, 0, frame.getWidth() - 14, menuBar.getHeight());
				toolPanel.setBounds(0, menuBar.getHeight(), frame.getWidth() - 14, 50);
			}
		});

		frame.addWindowStateListener(new WindowStateListener() {
			@SuppressWarnings("static-access")
			public void windowStateChanged(WindowEvent e) {
				if ((e.getNewState() & frame.MAXIMIZED_BOTH) == frame.MAXIMIZED_BOTH) {
					scrollPane.setBounds(0, 75, frame.getWidth() - 14, frame.getHeight() - 114);
				} else if ((e.getNewState() & frame.ICONIFIED) == frame.ICONIFIED) {
					scrollPane.setBounds(0, 75, frame.getWidth() - 14, frame.getHeight() - 114);
				}
			}
		});

		drawingPanel.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				drag = true;
				dragLocation = e.getPoint();
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				drag = false;
				if (drawingPanel.getSize().getWidth() < 50) {
					drawingPanel.setSize(50, (int) drawingPanel.getSize().getHeight());
				}
				if (drawingPanel.getSize().getHeight() < 50) {
					drawingPanel.setSize((int) drawingPanel.getSize().getWidth(), 50);
				}
				drawingPanel.setPreferredSize(new Dimension((int) drawingPanel.getSize().getWidth(),
						(int) drawingPanel.getSize().getHeight()));
				scrollPane.validate();
			}
		});

		drawingPanel.addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseDragged(MouseEvent e) {
				if (drag) {
					if (dragLocation.getX() > drawingPanel.getWidth() - 10
							&& dragLocation.getY() > drawingPanel.getHeight() - 10) {
						drawingPanel.setSize(
								(int) (drawingPanel.getWidth() + (e.getPoint().getX() - dragLocation.getX())),
								(int) (drawingPanel.getHeight() + (e.getPoint().getY() - dragLocation.getY())));
						dragLocation = e.getPoint();
					}
				}
			}
		});
	}

	@SuppressWarnings("serial")
	private class DrawingBoard extends JComponent {

		public DrawingBoard() {

			this.addMouseListener(new MouseAdapter() {

				public void mousePressed(MouseEvent e) {
					if (!cutMode && !copyMode && !deleteMode) {
						if (shapeNum != 4) {
							drawStart = new Point(e.getX(), e.getY());
						} else { // selecting Triangle
							lastShape = null;
							if (drawStart == null) {
								drawStart = e.getPoint();
								repaint();
							} else if (trianglePoint == null) {
								trianglePoint = e.getPoint();
							} else if (drawEnd == null) {
								drawEnd = e.getPoint();
								int[] coordinates = { drawStart.x, drawStart.y, trianglePoint.x, trianglePoint.y,
										drawEnd.x, drawEnd.y };
								Shape s = new Triangle(coordinates);
								s.setColor(strokeColor);
								s.setFillColor(fillColor);
								de.addShape(s);
								if (de.redoListEmpty()) {
									redoBtn.setEnabled(false);
								}
								undoBtn.setEnabled(true);
								drawStart = drawEnd = trianglePoint = null;
								repaint();
							}
						}
					}
					
					// Editing:

					if (cutMode || copyMode || deleteMode) { 
						Shape[] shapesList = de.getShapes();
						Shape selectedShape = null;
						for (Shape r : shapesList) { 
							// selecting the last shape added this point.
							if (((MyShape) r).contains(e.getPoint())) {
								selectedShape = r;
							}
						}
						lastShape = selectedShape;
						drawStart = e.getPoint();
						if (deleteMode) {
							de.removeShape(lastShape);
							drawStart = drawEnd = null;
							repaint();
						}
					}
				}

				public void mouseReleased(MouseEvent e) {
					// Drawing Shapes
					if (!cutMode && !copyMode && !deleteMode) {
						if (shapeNum != 4) {
							drawEnd = new Point(e.getX(), e.getY());
							Shape s;
							switch (shapeNum) {
							case 1:
								s = new LineSegment(drawStart.x, drawStart.y, drawEnd.x, drawEnd.y);
								break;
							case 2:
								s = new Ellipse(drawStart.x, drawStart.y, drawEnd.x, drawEnd.y);
								break;
							case 3:
								s = new Circle(drawStart.x, drawStart.y, drawEnd.x, drawEnd.y);
								break;
							case 5:
								s = new Rectangle(drawStart.x, drawStart.y, drawEnd.x, drawEnd.y);
								break;
							case 6:
								s = new Square(drawStart.x, drawStart.y, drawEnd.x, drawEnd.y);
								break;
							default:
								s = new LineSegment(drawStart.x, drawStart.y, drawEnd.x, drawEnd.y);
								break;
							}
							s.setColor(strokeColor);
							s.setFillColor(fillColor);
							// adding the shape to the drawing Engine
							de.addShape(s);
							if (de.redoListEmpty()) {
								redoBtn.setEnabled(false);
							}
							undoBtn.setEnabled(true);
							// undoBtn.setFocusable(true);
							drawStart = drawEnd = null;
							// Refresh
							repaint();
						}
					}

					if ((cutMode || copyMode) && lastShape != null) {
						drawEnd = e.getPoint();
						// the offset
						Point movement = new Point(drawEnd.x - drawStart.x, drawEnd.y - drawStart.y);
						Map<String, Double> properties = lastShape.getProperties();
						drawStart = new Point(properties.get("x1").intValue(), properties.get("y1").intValue());
						drawEnd = new Point(properties.get("x2").intValue(), properties.get("y2").intValue());
						Shape s;
						switch (properties.get("type").intValue()) {
						case 1:
							s = new LineSegment(drawStart.x + movement.x, drawStart.y + movement.y,
									drawEnd.x + movement.x, drawEnd.y + movement.y);
							break;
						case 2:
							s = new Ellipse(drawStart.x + movement.x, drawStart.y + movement.y, drawEnd.x + movement.x,
									drawEnd.y + movement.y);
							break;
						case 3:
							s = new Circle(drawStart.x + movement.x, drawStart.y + movement.y, drawEnd.x + movement.x,
									drawEnd.y + movement.y);
							break;
						case 5:
							s = new Rectangle(drawStart.x + movement.x, drawStart.y + movement.y,
									drawEnd.x + movement.x, drawEnd.y + movement.y);
							break;
						case 6:
							s = new Square(drawStart.x + movement.x, drawStart.y + movement.y, drawEnd.x + movement.x,
									drawEnd.y + movement.y);
							break;
						case 4:
							trianglePoint = new Point(properties.get("x3").intValue(), properties.get("y3").intValue());
							int[] coordinates = { drawStart.x + movement.x, drawStart.y + movement.y,
									drawEnd.x + movement.x, drawEnd.y + movement.y, trianglePoint.x + movement.x,
									trianglePoint.y + movement.y };
							s = new Triangle(coordinates);
							break;
						default:
							s = new LineSegment(drawStart.x + movement.x, drawStart.y + movement.y,
									drawEnd.x + movement.x, drawEnd.y + movement.y);
							break;
						}
						s.setColor(lastShape.getColor());
						s.setFillColor(lastShape.getFillColor());
						if (cutMode) {
							// removing old shape and adding the new one
							de.updateShape(lastShape, s);
						} else if (copyMode){
							// adding the new shape
							de.addShape(s);
						}
						if (de.redoListEmpty()) {
							redoBtn.setEnabled(false);
						}
						undoBtn.setEnabled(true);
						// undoBtn.setFocusable(true);
						drawStart = drawEnd = null;
						// Refresh
						repaint();
					}

				}
			});
			this.addMouseMotionListener(new MouseMotionAdapter() {

				public void mouseDragged(MouseEvent e) { 
					// Drawing the indicator Shape.
					if (!(cutMode || copyMode || deleteMode)) {
						if (shapeNum != 4) {
							drawEnd = new Point(e.getX(), e.getY());
							// selecting the shape based on the combo box
							Shape s;
							switch (shapeNum) {
							case 1:
								s = new LineSegment(drawStart.x, drawStart.y, drawEnd.x, drawEnd.y);
								break;
							case 2:
								s = new Ellipse(drawStart.x, drawStart.y, drawEnd.x, drawEnd.y);
								break;
							case 3:
								s = new Circle(drawStart.x, drawStart.y, drawEnd.x, drawEnd.y);
								break;
							case 5:
								s = new Rectangle(drawStart.x, drawStart.y, drawEnd.x, drawEnd.y);
								break;
							case 6:
								s = new Square(drawStart.x, drawStart.y, drawEnd.x, drawEnd.y);
								break;
							default:
								s = new LineSegment(drawStart.x, drawStart.y, drawEnd.x, drawEnd.y);
								break;
							}
							// saving the indicator shape
							lastShape = s;
							// Refresh
							repaint();
						}
					}
				}
			});
		}

		public void paint(Graphics g) {
			canvas = (Graphics2D) g;

			if (drawStart == null && drawEnd == null) {
				// after mouse release
				// redrawing the canvas with the added shape
				de.refresh(canvas);
			} else if (lastShape != null) {
				// while dragging
				// redrawing the canvas with the added shape
				de.refresh(canvas);
				// drawing the indicator shape
				canvas.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.40f));
				lastShape.setColor(Color.LIGHT_GRAY);
				lastShape.setFillColor(Color.LIGHT_GRAY);
				lastShape.draw(canvas);
			} else {
				de.refresh(canvas);
			}
		}
	}
}
