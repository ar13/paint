package paint;

import iPaint.Shape;

public class Command {
	final public String operation;
	final public Shape shape1;
	final public Shape shape2;
	
	public Command(final String operation, final Shape shape) {
		this.operation = operation;
		this.shape1 = shape;
		this.shape2 = null;
	}
	
	public Command(final String operation, final Shape shape1, final Shape shape2) {
		this.operation = operation;
		this.shape1 = shape1;
		this.shape2 = shape2;
	}
}
