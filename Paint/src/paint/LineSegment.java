package paint;

import java.awt.BasicStroke;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.geom.*;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

/**
 * A class to implement the drawing of a Line Segment.
 */
public class LineSegment extends MyShape {

	Shape shape;

	/**
	 * constructor to initialize the properties of a Line Segment.
	 */
	public LineSegment() {
		Map<String, Double> properties = new HashMap<String, Double>();
		final double initialLocation = 0.0;
		properties.put("type", 1.0);
		properties.put("x1", initialLocation);
		properties.put("y1", initialLocation);
		properties.put("x2", initialLocation);
		properties.put("y2", initialLocation);
		this.setProperties(properties);
	}

	/**
	 * constructor to put the actual properties of a Line Segment.
	 * 
	 * @param x1
	 *            x of the start point.
	 * @param y1
	 *            y of the start point.
	 * @param x2
	 *            x of the end point.
	 * @param y2
	 *            y of the end point.
	 */
	public LineSegment(final int x1, final int y1, final int x2, final int y2) {
		Map<String, Double> properties = new HashMap<String, Double>();
		properties.put("type", 1.0);
		properties.put("x1", (double) x1);
		properties.put("y1", (double) y1);
		properties.put("x2", (double) x2);
		properties.put("y2", (double) y2);
		this.setProperties(properties);
	}

	@Override
	final public void draw(Graphics canvas) {
		((Graphics2D) canvas).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		final int basicStroke = 4;
		((Graphics2D) canvas).setStroke(new BasicStroke(basicStroke));
		((Graphics2D) canvas).setPaint(this.getColor());
		Map<String, Double> properties = this.getProperties();
		final int x1 = properties.get("x1").intValue();
		final int y1 = properties.get("y1").intValue();
		final int x2 = properties.get("x2").intValue();
		final int y2 = properties.get("y2").intValue();
		Point2D start = new Point(x1, y1);
		Point2D end = new Point(x2, y2);
		Shape lineSegment = new Line2D.Double(start, end);
		((Graphics2D) canvas).draw(lineSegment);
		((Graphics2D) canvas).setPaint(this.getFillColor());
		((Graphics2D) canvas).fill(lineSegment);
		shape = lineSegment;
	}

	@Override
	final public Object clone() throws CloneNotSupportedException {
		MyShape newLineSegment = new LineSegment();
		newLineSegment.setColor(this.getColor());
		newLineSegment.setFillColor(this.getFillColor());
		newLineSegment.setPosition(this.getPosition());
		HashMap<String, Double> newprop = new HashMap<String, Double>();
		for (Iterator<Entry<String, Double>> iterator = this.getProperties().entrySet().iterator(); iterator
				.hasNext();) {
			Entry<String, Double> entry = iterator.next();
			newprop.put((String) entry.getKey(), (Double) entry.getValue());
		}
		newLineSegment.setProperties(newprop);
		return newLineSegment;
	}

	@Override
	public boolean contains(Point p) {
		if (shape != null) {
			return shape.intersects(p.x-5, p.y-5, 10, 10);
		}
		return false;
	}
}
