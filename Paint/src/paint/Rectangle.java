package paint;

import java.awt.BasicStroke;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.geom.*;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

/**
 * A class to implement the drawing of a rectangle.
 */
public class Rectangle extends MyShape {
	
	private Shape shape;
	/**
	 * constructor to initialize the properties of a rectangle.
	 */
	public Rectangle(){
		// initialize a dot 
        Map<String,Double> properties = new HashMap<String,Double>();
        final double initialLocation = 0.0;
        properties.put("type", 5.0);
		properties.put("x1", initialLocation);
		properties.put("y1", initialLocation);
		properties.put("x2", initialLocation);
		properties.put("y2", initialLocation);
        this.setProperties(properties);
	}
	
	/**
	 * constructor to put the actual properties of a rectangle.
	 * 
	 * @param x1
	 *            x of the upper left corner point.
	 * @param y1
	 *            y of the upper left corner point.
	 * @param x2
	 *            x of the lower right corner point.
	 * @param y2
	 *            y of the lower right corner point.
	 */
	public Rectangle(final int x1,final int y1,final int x2,final int y2){
			this.setPosition(new Point(x1,y1));
            
            Map<String,Double> properties = new HashMap<String,Double>();
            properties.put("type", 5.0);
            properties.put("x1", (double)x1);
            properties.put("y1", (double)y1);
            properties.put("x2",(double)x2);
            properties.put("y2",(double)y2);
            this.setProperties(properties);
	}
	
	@Override
	public void draw(Graphics canvas) {
		((Graphics2D) canvas).setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
		final int basicStroke = 4;
		((Graphics2D) canvas).setStroke(new BasicStroke(basicStroke));
		((Graphics2D) canvas).setPaint(this.getColor());
		Map<String, Double> properties = this.getProperties();
		double width = properties.get("x2") - properties.get("x1");
        double height = properties.get("y2") - properties.get("y1");
		double x = properties.get("x1");
		double y = properties.get("y1");
		final int zero = 0;
		if (width < zero) {
			x += width;
			width = - width;
		}
		if(height < zero) {
			y += height;
			height = - height;
		}
		Shape s = new Rectangle2D.Double(x, y, width, height);
		((Graphics2D) canvas).draw(s);
		((Graphics2D) canvas).setPaint(this.getFillColor());
		((Graphics2D) canvas).fill(s);
		
		shape = s;
	}
	
	@Override
	final public Object clone() throws CloneNotSupportedException {
		MyShape r = new Rectangle();
		r.setColor(this.getColor());
        r.setFillColor(this.getFillColor());
        r.setPosition(this.getPosition());
        HashMap<String,Double> newprop = new HashMap<String,Double>();
        for (Iterator<Entry<String, Double>> iterator = this.getProperties().entrySet().iterator(); iterator.hasNext();) {
			Entry<String, Double> s = iterator.next();
			newprop.put((String)s.getKey(), (Double)s.getValue());
		}
        r.setProperties(newprop);
        return r;
	}
	
	public boolean contains(Point p){
		if(shape != null){
			return shape.contains(p);
		}
		return false;
	}
}
