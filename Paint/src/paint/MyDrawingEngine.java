package paint;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.parsers.DocumentBuilder;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Attr;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import iPaint.DrawingEngine;
import iPaint.Shape;

public class MyDrawingEngine implements DrawingEngine {

	private ArrayList<Shape> shapes = new ArrayList<Shape>();
	private List<Command> undoList = new ArrayList<Command>();
	private List<Command> redoList = new ArrayList<Command>();
	private final int undoListMaxSize = 20;
	private final int firstIndex = 0;

	private Shape newShape;

	final public boolean undoListEmpty() {
		return undoList.isEmpty();
	}

	final public boolean redoListEmpty() {
		return redoList.isEmpty();
	}

	final private void undoLimit() {
		if (undoList.size() > undoListMaxSize) {
			undoList.remove(firstIndex);
		}
	}

	@Override
	public void refresh(Graphics canvas) {
		for (Shape s : shapes) {
			s.draw(canvas);
		}
	}

	@Override
	public void addShape(Shape shape) {
		shapes.add(shape);
		redoList.clear();
		Command command = new Command("add", shape);
		undoList.add(command);
		undoLimit();
	}

	@Override
	public void removeShape(Shape shape) {
		shapes.remove(shape);
		redoList.clear();
		Command command = new Command("remove", shape);
		undoList.add(command);
		undoLimit();
	}

	@Override
	public void updateShape(Shape oldShape, Shape newShape) {
		shapes.remove(oldShape);
		shapes.add(newShape);
		redoList.clear();
		Command command = new Command("update", oldShape, newShape);
		undoList.add(command);
		undoLimit();
	}

	@Override
	public Shape[] getShapes() {
		Shape[] result = shapes.toArray(new Shape[shapes.size()]);
		return result;
	}

	@Override
	public List<Class<? extends Shape>> getSupportedShapes() {
		Class<? extends Shape> lineSegment = LineSegment.class;
		Class<? extends Shape> ellipse = Ellipse.class;
		Class<? extends Shape> circle = Circle.class;
		Class<? extends Shape> triangle = Triangle.class;
		Class<? extends Shape> rectangle = Rectangle.class;
		Class<? extends Shape> square = Square.class;
		List<Class<? extends Shape>> supportedShapes = new ArrayList<Class<? extends Shape>>();
		supportedShapes.add(lineSegment);
		supportedShapes.add(ellipse);
		supportedShapes.add(circle);
		supportedShapes.add(triangle);
		supportedShapes.add(rectangle);
		supportedShapes.add(square);
		
		return supportedShapes;
	}

	@Override
	public void undo() {
		final int lastElement = undoList.size() - 1;
		Command command = undoList.get(lastElement);
		undoList.remove(lastElement);
		if (command.operation.equals("add")) {
			shapes.remove(command.shape1);
		} else if (command.operation.equals("remove")) {
			shapes.add(command.shape1);
		} else if (command.operation.equals("update")) {
			shapes.remove(command.shape2);
			shapes.add(command.shape1);
		}
		redoList.add(command);
	}

	@Override
	public void redo() {
		final int lastElement = redoList.size() - 1;
		Command command = redoList.get(lastElement);
		redoList.remove(lastElement);
		if (command.operation.equals("add")) {
			shapes.add(command.shape1);
		} else if (command.operation.equals("remove")) {
			shapes.remove(command.shape1);
		} else if (command.operation.equals("update")) {
			shapes.remove(command.shape1);
			shapes.add(command.shape2);
		}
		undoList.add(command);
		undoLimit();
	}

	@Override
	public void save(String path) {
		final String jsonExtension = path.substring(path.length() - 4, path.length());
		final String xmlExtension = path.substring(path.length() - 3, path.length());
		File file = new File(path);
		try {
			file.createNewFile();
		} catch (Exception e) {
			throw new RuntimeException();
		}
		if (jsonExtension.equals("json")) {
			saveJson(path);
		} else if (xmlExtension.equals("xml")) {
			try {
				saveXML(path);
			} catch (Exception e) {
				throw new RuntimeException();
			}
		}
	}

	private void saveXML(String path) throws Exception {
		// save the shapes in xml file.
		// making new document
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document d = db.newDocument();

		// putting shapes in document.
		Shape[] shapes = this.getShapes();
		Element root = d.createElement("Shapes");
		d.appendChild(root);
		// iterating shapes.
		for (int i = 0; i < shapes.length; i++) {
			if (shapes[i] != null) {
				Element shape = d.createElement("shape");
				root.appendChild(shape);
				// Class of the shape object.
				Class<? extends Shape> shapeClass = shapes[i].getClass();
				String ShapeClassName = shapeClass.getName();
				Attr shapeType = d.createAttribute("Class");
				shapeType.setTextContent(ShapeClassName);
				shape.setAttributeNode(shapeType);

				Attr attr = d.createAttribute("Position");
				String position = String.valueOf(shapes[i].getPosition());
				attr.setTextContent(position);
				shape.setAttributeNode(attr);

				Attr attr1 = d.createAttribute("Color");
				String color = String.valueOf(shapes[i].getColor().getRGB());
				attr1.setTextContent(color);
				shape.setAttributeNode(attr1);

				Attr attr2 = d.createAttribute("fillColor");
				String fillColor = String.valueOf(shapes[i].getFillColor().getRGB());
				attr2.setTextContent(fillColor);
				shape.setAttributeNode(attr2);

				// iterating properties
				Element propertiesNode = d.createElement("Properties");
				shape.appendChild(propertiesNode);
				Map<String, Double> properties = shapes[i].getProperties();
				for (Entry<String, Double> key : properties.entrySet()) {
					String k = String.valueOf(key.getKey());
					Element propertyNode = d.createElement(k);
					String property = String.valueOf(key.getValue());
					propertyNode.setTextContent(property);
					propertiesNode.appendChild(propertyNode);
				}
			}
		}

		// saving the document to the chosen file.
		TransformerFactory tf = TransformerFactory.newInstance();
		Transformer t = tf.newTransformer();
		DOMSource s = new DOMSource(d);
		StreamResult sr = new StreamResult(new FileOutputStream(path));
		t.transform(s, sr);
	}

	final private void saveJson(final String path) {
		// creating the file text.
		StringBuilder jsonText = new StringBuilder();
		jsonText.append("{ \"shapes\": [\n  ");
		final int shapesLength = this.shapes.size();
		Shape shape;
		Map<String, Double> properties;
		double type;
		for (int i = 0; i < shapesLength; i++) {
			shape = this.shapes.get(i);
			properties = shape.getProperties();
			if (shape != null) {
				jsonText.append("\t{\"type\": ");
				type = properties.get("type");
				jsonText.append((int) type);
				final String color = String.valueOf(shape.getColor().getRGB());
				final String fillColor = String.valueOf(shape.getFillColor().getRGB());
				jsonText.append(", \"color\": " + color);
				jsonText.append(", \"fill\": " + fillColor);
				for (Entry<String, Double> entry : properties.entrySet()) {
					String name = String.valueOf(entry.getKey());
					if (!name.equals("type")) {
						String value = String.valueOf(entry.getValue().intValue());
						jsonText.append(", \"" + name + "\": " + value);
					}
				}
				jsonText.append("}");
				if (i != this.shapes.size() - 1 && this.shapes.get(i + 1) != null) {
					jsonText.append(",");
				}
				jsonText.append("\n  ");
			}
		}
		jsonText.append("]\n}");

		// saving the text in a .json file
		File file = new File(path);
		try {
			if (file.exists()) {
				file.delete();
			}
			file.createNewFile();
			FileWriter fileWriter = new FileWriter(file, true);
			BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
			bufferedWriter.write(jsonText.toString());
			bufferedWriter.flush();
			bufferedWriter.close();
		} catch (IOException e) {
		}
	}

	@Override
	public void load(String path) {
		File test = new File(path);
		if (!test.exists()) {
			throw new RuntimeException();
		}
		final String jsonExtension = path.substring(path.length() - 4, path.length());
		final String xmlExtension = path.substring(path.length() - 3, path.length());
		if (jsonExtension.equals("json")) {
			try {
				Scanner file = new Scanner(new File(path));
				file.close();
			} catch(Exception e) {
				throw new RuntimeException();
			}
			loadJson(path);
		} else if (xmlExtension.equals("xml")) {
			try {
				loadXML(path);
			} catch (Exception e) {
				throw new RuntimeException();
			}
		} //else {
			//throw new RuntimeException();
		//}
	}

	private void loadXML(String path) throws Exception { // not working yet.
		// load shapes from xml file.
		File xmlFile = new File(path);
		this.shapes.clear();
		this.undoList.clear();
		this.redoList.clear();
		// loading file to document.
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document d = db.parse(xmlFile);

		// getting shapes from document.
		Node root = d.getFirstChild();
		NodeList list = root.getChildNodes();

		// iterate over shapes.
		for (int i = 0; i < list.getLength(); i++) {
			Node node = list.item(i);
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				Element shapeElement = (Element) node;

				// Class
				String className = shapeElement.getAttribute("Class");
				ClassLoader cls = ClassLoader.getSystemClassLoader();
				Class<?> shapeClass = cls.loadClass(className);
				newShape = (Shape) shapeClass.newInstance();

				// position
				String p = shapeElement.getAttribute("Position");
				p = p.replaceAll("[a-z A-Z\\[\\]\\.=]", "");
				String[] xy = p.split(",");
				if (xy[0].matches("\\d")&&xy[1].matches("\\d")) {
					Point position = new Point(Integer.parseInt(xy[0]), Integer.parseInt(xy[1]));
					newShape.setPosition(position);
				}
				// color
				Color color = new Color(Integer.parseInt(shapeElement.getAttribute("Color")));
				newShape.setColor(color);

				// fillcolor
				Color fillColor = new Color(Integer.parseInt(shapeElement.getAttribute("fillColor")));
				newShape.setFillColor(fillColor);

				// properties
				Element porpertiesElement = (Element) shapeElement.getFirstChild();
				NodeList propertiesList = porpertiesElement.getChildNodes();
				Map<String, Double> properties = new HashMap<String, Double>();

				// iterate over properties.
				for (int j = 0; j < propertiesList.getLength(); j++) {
					Node propertyNode = propertiesList.item(j);

					if (propertyNode.getNodeType() == Node.ELEMENT_NODE) {
						Element propertyElement = (Element) propertyNode;

						properties.put(propertyElement.getTagName(), Double.valueOf(propertyElement.getTextContent()));
					}
				}
				newShape.setProperties(properties);

				if (newShape != null) {
					// putting shape in arraylist.
					//this.addShape(newShape);
					this.shapes.add(newShape);
				}
			}
		}
	}
	
	final private void loadJson(final String path) {
		Scanner file;
		try {
			file = new Scanner(new File(path));
			this.shapes.clear();
			this.undoList.clear();
			this.redoList.clear();
			file.nextLine();
			while (file.hasNext()) {
				String line = file.nextLine();
				if(line.equals("  ]")) {
					break;
				}
				line = line.substring(3, line.length());
				final String[] entries = line.split(",");
				final int length = entries.length;
				Map<String, Double> properties = new HashMap<String, Double>();
				Color color = null, fillColor = null;
				for (int i = 0; i < length; i++) {
					String temp = entries[i];
					String name;
					int value;
					if (!temp.equals("")) {
						String[] tokens = temp.split(":");
						name = tokens[0].substring(2, tokens[0].length() - 1);
						String temp1 = tokens[1].substring(1, tokens[1].length());
						if(temp1.charAt(temp1.length() - 1) == '}') {
							temp1 = temp1.substring(0, temp1.length() - 1);
						}
						value = Integer.parseInt(temp1);
						if(name.equals("color")) {
							color = new Color(value);
						} else if(name.equals("fill")) {
							fillColor = new Color(value);
						} else {
							properties.put(name, (double) value);
						}
					}
				}
				int val = properties.get("type").intValue();
				Shape shape;
				switch(val) {
				case 1:
					shape = new LineSegment();
					break;
				case 2:
					shape = new Ellipse();
					break;
				case 3:
					shape = new Circle();
					break;
				case 4:
					shape = new Triangle();
					break;
				case 5:
					shape = new Rectangle();
					break;
				case 6:
					shape = new Square();
					break;
				default:
					shape = new LineSegment();
					break;
				}
				shape.setProperties(properties);
				shape.setColor(color);
				shape.setFillColor(fillColor);
				if (shape != null) {
					this.shapes.add(shape);
					//addShape(shape);
				}
			}	
		} catch(Exception e){
		}
	}
}
