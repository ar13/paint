package paint;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.util.Map;

import iPaint.Shape;

public abstract class MyShape implements Shape {

	private Point position;
	private Map<String, Double> properties;
	private Color color = Color.BLACK, fillColor = Color.BLACK;

	public void setPosition(final Point position) {
		this.position = position;
	}

	public Point getPosition() {
		return this.position;
	}

	public void setProperties(final Map<String, Double> properties) {
		this.properties = properties;
	}

	public Map<String, Double> getProperties() {
		return this.properties;
	}

	public void setColor(final Color color) {
		this.color = color;
	}

	public Color getColor() {
		return this.color;
	}

	@Override
	public void setFillColor(Color color) {
		this.fillColor = color;
	}

	@Override
	public Color getFillColor() {
		return this.fillColor;
	}

	@Override
	public abstract void draw(Graphics canvas);

	@Override
	public abstract Object clone() throws CloneNotSupportedException;

	public abstract boolean contains(Point p);
}
