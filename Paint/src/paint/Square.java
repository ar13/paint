package paint;

import java.awt.BasicStroke;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.geom.*;
import java.util.Map;

/**
 * A class to implement the drawing of a square.
 */
public class Square extends Rectangle {

	private Shape shape;
	/**
	 * x coordinate of the end point.
	 */
	final private int x2;
	
	/**
	 * y coordinate of the end point.
	 */
	final private int y2;
	
	/**
	 * constructor to initialize the properties of a square.
	 */
	public Square() {
		super();
		this.x2 = 0;
		this.y2 = 0;
	}

	/**
	 * constructor to put the actual properties of a square.
	 * 
	 * @param x1
	 *            x of the upper left corner point.
	 * @param y1
	 *            y of the upper left corner point.
	 * @param x2
	 *            x of the lower right corner point.
	 * @param y2
	 *            y of the lower right corner point.
	 */
	public Square(final int x1, final int y1, final int x2, final int y2) {
		super(x1, y1, x2, y2);
		this.x2 = x2;
		this.y2 = y2;
		Map<String, Double> properties = this.getProperties();
		properties.put("type", 6.0);
	}
	
	@Override
	final public void draw(Graphics canvas) {
		((Graphics2D) canvas).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		final int basicStroke = 4;
		((Graphics2D) canvas).setStroke(new BasicStroke(basicStroke));
		((Graphics2D) canvas).setPaint(this.getColor());
		Map<String, Double> properties = this.getProperties();
		double width = properties.get("x2") - properties.get("x1");
        double height = properties.get("y2") - properties.get("y1");
		double x = properties.get("x1");
		double y = properties.get("y1");
		if (y > this.y2) {	
			if (this.x2 > x){
				y += height;
				height = - height;
			}
			width = height;
		}
		final int nonNegativeNonPositive = 0;
		if (width < nonNegativeNonPositive) {
			x += width;
			width = - width;	
		}
		if (height < nonNegativeNonPositive) {
			y += height;
		}
		Shape square = new Rectangle2D.Double(x, y, width, width);
		((Graphics2D) canvas).draw(square);
		((Graphics2D) canvas).setPaint(this.getFillColor());
		((Graphics2D) canvas).fill(square);
		shape = square;
	}
	
	public boolean contains(Point p){
		if(shape != null){
			return shape.contains(p);
		}
		return false;
	}
}
