package paint;


import java.awt.BasicStroke;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

/**
 * A class to implement the drawing of a triangle.
 */
public class Triangle extends MyShape {

	private Shape shape;
	/**
	 * constructor to initialize the properties of a triangle.
	 */
	public Triangle() {
		Map<String, Double> properties = new HashMap<String, Double>();
		final double initialLocation = 0.0;
		properties.put("type", 4.0);
		properties.put("x1", initialLocation);
		properties.put("y1", initialLocation);
		properties.put("x2", initialLocation);
		properties.put("y2", initialLocation);
		properties.put("x3", initialLocation);
		properties.put("y3", initialLocation);
		this.setProperties(properties);
	}
	
	/**
	 * 
	 * @param coordinates the coordinates array.
	 */
	public Triangle(final int coordinates[]) {
		Map<String, Double> properties = new HashMap<String, Double>();
		properties.put("type", 4.0);
		properties.put("x1", (double) coordinates[0]);
		properties.put("y1", (double) coordinates[1]);
		properties.put("x2", (double) coordinates[2]);
		properties.put("y2", (double) coordinates[3]);
		properties.put("x3", (double) coordinates[4]);
		properties.put("y3", (double) coordinates[5]);
		this.setProperties(properties);
	}
	
	@Override
	final public void draw(Graphics canvas) {
		((Graphics2D) canvas).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		final int basicStroke = 4;
		((Graphics2D) canvas).setStroke(new BasicStroke(basicStroke));
		((Graphics2D) canvas).setPaint(this.getColor());
		Map<String, Double> properties = this.getProperties();
		final int x1 = properties.get("x1").intValue();
		final int y1 = properties.get("y1").intValue();
		final int x2 = properties.get("x2").intValue();
		final int y2 = properties.get("y2").intValue();
		final int x3 = properties.get("x3").intValue();
		final int y3 = properties.get("y3").intValue();
		Shape triangle = new Polygon(new int[] {x1, x2, x3},new int[] {y1, y2, y3}, 3);
		((Graphics2D) canvas).draw(triangle);
		((Graphics2D) canvas).setPaint(this.getFillColor());
		((Graphics2D) canvas).fill(triangle);
		shape = triangle;
	}

	@Override
	final public Object clone() throws CloneNotSupportedException {
		MyShape triangle = new Triangle();
		triangle.setColor(this.getColor());
		triangle.setFillColor(this.getFillColor());
		triangle.setPosition(this.getPosition());
        HashMap<String,Double> newprop = new HashMap<String,Double>();
        for (Iterator<Entry<String, Double>> iterator = this.getProperties().entrySet().iterator(); iterator.hasNext();) {
			Entry<String, Double> entry = iterator.next();
			newprop.put((String)entry.getKey(), (Double)entry.getValue());
		}
        triangle.setProperties(newprop);
        return triangle;
	}

	@Override
	public boolean contains(Point p) {
		if(shape != null){
			return shape.contains(p);
		}
		return false;
	}
}
