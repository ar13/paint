package paint;

import java.awt.BasicStroke;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.geom.*;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import static java.lang.Math.min;
import static java.lang.Math.abs;

/**
 * A class to implement the drawing of an ellipse.
 */
public class Ellipse extends MyShape {

	Shape shape;
	/**
	 * constructor to initialize the properties of an ellipse.
	 */
	public Ellipse() {
		Map<String, Double> properties = new HashMap<String, Double>();
		final double initialLocation = 0.0;
		properties.put("type", 2.0);
		properties.put("x1", initialLocation);
		properties.put("y1", initialLocation);
		properties.put("x2", initialLocation);
		properties.put("y2", initialLocation);
		this.setProperties(properties);
	}

	/**
	 * constructor to put the actual properties of an ellipse.
	 * 
	 * @param x1
	 *            x of the upper left corner point.
	 * @param y1
	 *            y of the upper left corner point.
	 * @param x2
	 *            x of the lower right corner point.
	 * @param y2
	 *            y of the lower right corner point.
	 */
	public Ellipse(final int x1, final int y1, final int x2, final int y2) {
		Map<String, Double> properties = new HashMap<String, Double>();
		properties.put("type", 2.0);
		properties.put("x1", (double) x1);
		properties.put("y1", (double) y1);
		properties.put("x2", (double) x2);
		properties.put("y2", (double) y2);
		this.setProperties(properties);
	}

	@Override
	public void draw(Graphics canvas) {
		((Graphics2D) canvas).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		final int basicStroke = 4;
		((Graphics2D) canvas).setStroke(new BasicStroke(basicStroke));
		((Graphics2D) canvas).setPaint(this.getColor());
		Map<String, Double> properties = this.getProperties();
		final double cornerX = min(properties.get("x1"), properties.get("x2"));
		final double cornerY = min(properties.get("y1"), properties.get("y2"));
		final double width = abs(properties.get("x1") - properties.get("x2"));
		final double height = abs(properties.get("y1") - properties.get("y2"));
		Shape ellipse = new Ellipse2D.Double(cornerX, cornerY, width, height);
		((Graphics2D) canvas).draw(ellipse);
		((Graphics2D) canvas).setPaint(this.getFillColor());
		((Graphics2D) canvas).fill(ellipse);
		shape = ellipse;
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		MyShape newEllipse = new Circle();
		newEllipse.setColor(this.getColor());
		newEllipse.setFillColor(this.getFillColor());
		newEllipse.setPosition(this.getPosition());
		HashMap<String, Double> newprop = new HashMap<String, Double>();
		for (Iterator<Entry<String, Double>> iterator =
				this.getProperties().entrySet().iterator(); iterator.hasNext();) {
			Entry<String, Double> entry = iterator.next();
			newprop.put((String) entry.getKey(), (Double) entry.getValue());
		}
		newEllipse.setProperties(newprop);
		return newEllipse;
	}

	@Override
	public boolean contains(Point p) {
		if(shape != null){
			return shape.contains(p);
		}
		return false;
	}
}
